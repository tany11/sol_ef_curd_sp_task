﻿using Sol_Ef_Sp_Curd_Task.CommonRepository;
using Sol_Ef_Sp_Curd_Task.Entity.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Repository.Interface
{
    public interface IUserRepository : IInsert<IUserEntity>, IUpdate<IUserEntity>, IDelete<IUserEntity>
    {
    }
}
