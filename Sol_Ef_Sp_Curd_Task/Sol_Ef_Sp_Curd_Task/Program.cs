﻿using Sol_Ef_Sp_Curd_Task.Entity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                UserDal userDalObj = new UserDal();

                #region Insert Section

                Boolean flag = await userDalObj.AddAsync(new UserEntity()
                {
                    FirstName = "Kishor",
                    LastName = "Naik"
                });


                #endregion

                #region Update Section
                //Boolean flag = await userDalObj.UpdateAsync(new UserEntity()
                //{
                //    UserId=3,
                //    FirstName = "Yogesh",
                //    LastName = "Naik"
                //});
                #endregion

                //#region Delete Section
                //Boolean flag = await userDalObj.DeleteAsync(new UserEntity()
                //{
                //    UserId = 3
                //});
                //#endregion 

            }).Wait();

        }
    }
}
