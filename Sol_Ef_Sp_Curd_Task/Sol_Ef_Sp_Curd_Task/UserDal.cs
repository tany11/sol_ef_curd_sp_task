﻿using Sol_Ef_Sp_Curd_Task.Concrete;
using Sol_Ef_Sp_Curd_Task.Entity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task
{
    public class UserDal
    {
        #region Declaration
        //private UserDBEntities db = null;

        private UserConcrete userConcreteObj = null;

        #endregion

        #region Constructor
        public UserDal()
        {
            //db = new UserDBEntities();
            userConcreteObj = new UserConcrete();
        }
        #endregion

        #region Public Methods
        public async Task<Boolean> AddAsync(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                await userConcreteObj.Set
                   ("Insert",
                   userEntityObj,
                   (leStatus, leMessage) =>
                   {
                       status = leStatus;
                       message = leMessage;
                   });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<Boolean> UpdateAsync(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            //ObjectParameter status = null;
            //ObjectParameter message = null;
            //try
            //{
            //    return await Task.Run(() => {

            //        var setQuery =
            //         db
            //         ?.uspSetUser(
            //             "Update",
            //             userEntityObj.UserId,
            //             userEntityObj.FirstName,
            //             userEntityObj.LastName,
            //             status = new ObjectParameter("Status", typeof(int)),
            //             message = new ObjectParameter("Message", typeof(string))
            //         );

            //        return (Convert.ToInt32(status.Value) == 1) ? true : false;

            //    });
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

            try
            {
                await userConcreteObj.Set
                   ("Update",
                   userEntityObj,
                   (leStatus, leMessage) =>
                   {
                       status = leStatus;
                       message = leMessage;
                   });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Boolean> DeleteAsync(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            //ObjectParameter status = null;
            //ObjectParameter message = null;
            //try
            //{
            //    return await Task.Run(() => {

            //        var setQuery =
            //         db
            //         ?.uspSetUser(
            //             "Delete",
            //             userEntityObj.UserId,
            //             userEntityObj.FirstName,
            //             userEntityObj.LastName,
            //             status = new ObjectParameter("Status", typeof(int)),
            //             message = new ObjectParameter("Message", typeof(string))
            //         );

            //        return (Convert.ToInt32(status.Value) == 1) ? true : false;

            //    });
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            try
            {
                await userConcreteObj.Set
                   ("Delete",
                   userEntityObj,
                   (leStatus, leMessage) =>
                   {
                       status = leStatus;
                       message = leMessage;
                   });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion 
    }
}


