﻿using Sol_Ef_Sp_Curd_Task.Entity.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Entity.User
{
    public class UserEntity : IUserEntity
    {
        public int UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public UserCommunicationEntity UserCommunication { get; set; }

        public UserLoginEntity UserLogin { get; set; }
    }
}
