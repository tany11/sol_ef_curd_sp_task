﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Entity.User.Interface
{
    public interface IUserCommunicationEntity
    {
        int UserId { get; set; }

        String MobileNo { get; set; }

        String EmailId { get; set; }
    }
}
