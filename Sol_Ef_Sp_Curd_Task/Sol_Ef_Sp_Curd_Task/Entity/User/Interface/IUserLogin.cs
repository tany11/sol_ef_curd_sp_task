﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Entity.User.Interface
{
    public interface IUserLoginEntity
    {
        int UserId { get; set; }

        String UserName { get; set; }

        String Password { get; set; }
    }
}
