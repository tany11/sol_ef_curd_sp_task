﻿using Sol_Ef_Sp_Curd_Task.Entity.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Entity.User
{
    public class UserCommunicationEntity : IUserCommunicationEntity
    {
        public int UserId { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}
