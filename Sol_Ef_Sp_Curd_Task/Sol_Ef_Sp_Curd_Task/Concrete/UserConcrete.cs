﻿using Sol_Ef_Sp_Curd_Task.EF;
using Sol_Ef_Sp_Curd_Task.Entity.User;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.Concrete
{
    public class UserConcrete
    {
        #region Declaration
        private TaskDBEntities db = null;
        #endregion

        #region Constructor
        public UserConcrete()
        {
            db = new TaskDBEntities();
        }
        #endregion 

        public async Task<dynamic> Set(String command, UserEntity userEntityObj, Action<int?, string> actionStoredProcOut = null)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;
            try
            {
                return await Task.Run(() => {

                    var setQuery =
                     db
                     ?.uspSetUser(
                         command,
                         userEntityObj.UserId,
                         userEntityObj.FirstName,
                         userEntityObj.LastName,
                         status = new ObjectParameter("Status", typeof(int)),
                         message = new ObjectParameter("Message", typeof(string))
                     );

                    // get stored Procedure Out put parameter Value
                    actionStoredProcOut(Convert.ToInt32(status.Value), message.Value.ToString());

                    return setQuery;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

