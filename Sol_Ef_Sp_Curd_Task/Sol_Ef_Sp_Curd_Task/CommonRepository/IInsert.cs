﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.CommonRepository
{
    public interface IInsert<TEntity> where TEntity : class
    {
        Task<Boolean> Insert(TEntity entityObj);
    }
}
