﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Ef_Sp_Curd_Task.CommonRepository
{
    public interface IUpdate<TEntity> where TEntity : class
    {
        Task<Boolean> Update(TEntity entityObj);
    }
}
